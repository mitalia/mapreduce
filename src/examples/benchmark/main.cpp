#include <QCoreApplication>
#include <QtConcurrent/QtConcurrentMap>
#include <QElapsedTimer>
#include "mapreduce.hpp"
#include <iostream>

int identity(int i)
{
    return i;
}

long long fibo(long long i)
{
    if(i==0)
        return 0;
    else if(i==1)
        return 1;
    else
        return fibo(i-1) + fibo(i-2);
}

long long recufibo(long long i)
{
    if(i==0)
        return 0;
    else if(i==1)
        return 1;
    else
    {
        if(i>24 && i<30)
        {
        std::vector<long long> v;
        v.push_back(i-1);
        v.push_back(i-2);
        return MapReduce::mapReduce<long long>(
                    v,
                    &recufibo,
                    [](int a, int b) { return a+b; }
                    ).result();
        }
        else
        {
            return recufibo(i-1) + recufibo(i-2);
        }
    }
}

template<typename T>
void printV (std::ostream &os, const T &v)
{
    os<<"[";
    for(int i=0, n=int(v.size()); i<n; ++i)
        os<<v[i]<<((i==n-1)?"":", ");
    os<<"]";
}

template<typename F>
void doBench(const char *name, int count, const F &fn)
{
    decltype(fn()) r;
    QElapsedTimer t;
    t.start();
    for(int i=0; i<count; ++i)
    {
        auto res=fn();
        if(r.size()==0)
            r=res;
        else if(res!=r)
        {
            std::cout<<"r!=res";
            std::cout<<"\nr: ";
            printV(std::cout, r);
            std::cout<<"\nres: ";
            printV(std::cout, res);
            std::cout<<std::endl;
            std::abort();
        }
    }
    std::cout<<name<<" ("<<count<<"): "<<t.elapsed()<<" msec\n";
    printV(std::cout, r);
    std::cout<<std::endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    std::vector<int> numbers;
    for(int i=0; i<50; ++i)
        numbers.push_back(24 + i % 10);
    for(int i=0; i<2; ++i)
    {
        doBench("mr-identity", 40000, [&]() -> std::vector<long long> {
            return MapReduce::map<long long>(numbers, &identity).result();
        });
        doBench("qtconcurrent-identity", 40000, [&]()
        {
            return QtConcurrent::mapped(numbers, &identity).results();
        });
    }
    for(int i=0; i<2; ++i)
    {
        doBench("mr-fibo", 100, [&]() -> std::vector<long long> {
            return MapReduce::map<long long>(numbers, &fibo).result();
        });
        doBench("qtconcurrent-fibo", 100, [&]() {
            return QtConcurrent::mapped(numbers, &fibo).results();
        });
    }
    doBench("recu-fibo", 100, [&]() -> std::vector<long long> {
        return MapReduce::map<long long>(numbers, &recufibo).result();
    });
    return 0;
}
