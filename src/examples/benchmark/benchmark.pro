#-------------------------------------------------
#
# Project created by QtCreator 2014-08-16T18:59:08
#
#-------------------------------------------------

QT       += core concurrent

QT       -= gui

TARGET = benchmark
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp ../../mapreduce.cpp
INCLUDEPATH += ../../include

HEADERS += \
    ../../include/mapreduce.hpp

QMAKE_CXXFLAGS += -g

