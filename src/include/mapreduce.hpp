#ifndef MAPREDUCE_HPP
#define MAPREDUCE_HPP
#include <vector>
#include <QRunnable>
#include <QAtomicInt>
#include <QAtomicPointer>
#include <QSemaphore>
#include <QThreadPool>
#include <QSharedPointer>
#include <QThreadStorage>

namespace MapReduce
{
namespace Impl
{
/**
 * Default thread pool for the current thread; the double pointer is because
 * we don't want to cease its ownership to QThreadStorage
 */
extern QThreadStorage<QThreadPool **> defaultThreadPool;
}

enum CleanupMode
{
    NoDelete,   ///< don't cease ownership
    AutoDelete  ///< cease ownership
};

/**
 * SetDefThreadPool provides RAII-style scoped override for the default thread
 * pool of the current thread
 */
class SetDefThreadPool
{
    CleanupMode cleanupMode;    ///< if AutoDelete we got ownership of the new thread pool
    QThreadPool *oldThreadPool; ///< thread pool to restore on destruction
public:

    /**
     * Sets the current thread thread pool to %threadPool; acquires its ownership if
     * %cleanupMode is set to %AutoDelete
     */
    SetDefThreadPool(QThreadPool *threadPool, CleanupMode cleanupMode)
        : cleanupMode(cleanupMode), oldThreadPool(threadPool)
    {
        if(!Impl::defaultThreadPool.hasLocalData())
            Impl::defaultThreadPool.setLocalData(new QThreadPool *(NULL));
        std::swap(oldThreadPool, *Impl::defaultThreadPool.localData());
    }

    /**
     * Restores the old thread pool, deleting the new one if %cleanupMode was
     * set to %AutoDelete
     */
    ~SetDefThreadPool()
    {
        std::swap(*Impl::defaultThreadPool.localData(), oldThreadPool);
        if(oldThreadPool && cleanupMode == AutoDelete)
            delete oldThreadPool;
    }
private:
    // Disable heap allocation
    void *operator new( size_t );
    void operator delete( void* );
    void *operator new[]( size_t );
    void operator delete[]( void* );
    // Disable copy
    SetDefThreadPool(const SetDefThreadPool &);
};

namespace Impl
{

/**
 * Barebones runnable object; call %run and it will run
 */
struct BareRunnable
{
    virtual void run()=0;
    virtual ~BareRunnable() {}
};

/**
 * The BareRunnableWrapper wraps a QSharedPointer<BareRunnable> in an autodeleting
 * QRunnable.
 *
 * This allows keeping alive the %BareRunnable with the wrappers posted in the
 * QThreadPool and possibly with an external QSharedPointer.
 */
struct BareRunnableWrapper
        : QRunnable
{
    QSharedPointer<BareRunnable> ptr;

    BareRunnableWrapper(QSharedPointer<BareRunnable> ptr)
        : ptr(ptr)
    {
        this->setAutoDelete(true);
    }

    virtual void run() { ptr->run(); }
};


/**
 * Abstract runnable job with result of type %R; encapsulated by %Future (with
 * reference counting)
 */
template<typename R>
struct Job : BareRunnable
{
    virtual const R &result()=0;
};

template<>
struct Job<void> : BareRunnable
{
    virtual void result()=0;
};

/**
 * Base class for all jobs involving a map operation
 */
template<
        typename T,      ///< source type
        typename U,      ///< destination type
        typename TMapF,  ///< mapping function type
        typename ResultT>///< type of the result
struct BaseMapJob : public Job<ResultT>
{
    QAtomicInt nextElem; ///< next element to map
    std::vector<T> in;   ///< input sequence
    TMapF mapF;          ///< map function
    /**
     * Completion semaphore; starts at 1-in.size(), is incremented for each
     * element completed; when the last element is done, we get 1 above zero,
     * which allows %waitForFinish to proceed.
     */
    QSemaphore completion;
    QThreadPool *owner;

    BaseMapJob(const std::vector<T> &in, const TMapF &mapF, QThreadPool *owner) : in(in), mapF(mapF), completion(1-int(in.size())), owner(owner)
    {
    }

    virtual void run()
    {
        // Save the owner thread pool as default thread pool for the task
        SetDefThreadPool sdtp(owner, NoDelete);
        doRun();
    }

    /**
     * Blocks until the job has finished
     */
    void waitForFinish()
    {
        // Help finish the work - that is strictly necessary if the caller is
        // a thread of our thread pool (we may go in deadlock otherwise)
        SetDefThreadPool sdtp(owner, NoDelete);
        this->run();
        completion.acquire();
        completion.release();
    }

    virtual void doRun()=0;

    virtual ~BaseMapJob() {}
};


/**
 * [T] -> [U] map job
 */
template<typename T, typename U, typename TMapF>
struct MapJob : public BaseMapJob<T, U, TMapF, std::vector<U> >
{
    std::vector<U> mapped;    ///< target vector

    virtual void doRun()
    {
        // take the next element
        int i=this->nextElem.fetchAndAddOrdered(1);
        // the other threads may have already completed the queue
        if(i<int(this->in.size()))
        {
            int count=0;
            do
            {
                // do the mapping
                mapped[i]=this->mapF(this->in[i]);
                count++;
            } while((i=this->nextElem.fetchAndAddOrdered(1))<int(this->in.size()));
            this->completion.release(count);
        }
    }

    MapJob(const std::vector<T> &in, const TMapF &mapF, QThreadPool *owner) : BaseMapJob<T, U, TMapF, std::vector<U> >(in, mapF, owner)
    {
        mapped.resize(this->in.size());
    }

    virtual const std::vector<U> &result()
    {
        this->waitForFinish();
        return mapped;
    }
};

template<typename T, typename TMapF>
struct MapJob<T, void, TMapF> : public BaseMapJob<T, void, TMapF, void >
{
    virtual void doRun()
    {
        // take the next element
        int i=this->nextElem.fetchAndAddOrdered(1);
        // the other threads may have already completed the queue
        if(i<int(this->in.size()))
        {
            int count=0;
            do
            {
                // do the mapping
                this->mapF(this->in[i]);
                count++;
            } while((i=this->nextElem.fetchAndAddOrdered(1))<int(this->in.size()));
            this->completion.release(count);
        }

    }

    MapJob(const std::vector<T> &in, const TMapF &mapF, QThreadPool *owner) : BaseMapJob<T, void, TMapF, void >(in, mapF, owner)
    {
    }

    virtual void result()
    {
        this->waitForFinish();
    }
};

/**
 * [T] -> U map+reduce job
 */
template<typename T, typename U, typename TMapF, typename TRedF>
struct MapReduceJob : public BaseMapJob<T, U, TMapF, U>
{
    std::vector<U> redStack;    ///< stack where each thread puts the result of its reductions
    QAtomicInt stackTop;        ///< pointer to the current top of the stack
    TRedF redF;                 ///< reduction function
    QAtomicInt completedPartials;   ///< number of map operations performed

    virtual void doRun()
    {
        // take the next element
        int i=this->nextElem.fetchAndAddOrdered(1);
        // the other threads may have already completed the queue
        if(i<int(this->in.size()))
        {
            // thread-local reduced element
            U reduced=this->mapF(this->in[i]);
            int count=1;
            while((i=this->nextElem.fetchAndAddOrdered(1))<int(this->in.size()))
            {
                reduced=this->redF(reduced, this->mapF(this->in[i]));
                count++;
            }
            // put it on the stack
            redStack[stackTop.fetchAndAddOrdered(1)]=reduced;
            // add the completed partials
            completedPartials.fetchAndAddOrdered(count);

            // get in only if we finished
            if(completedPartials.fetchAndAddOrdered(0)==int(this->in.size()))
            {
                // only one thread will get in there
                if(completedPartials.fetchAndStoreOrdered(-1)!=-1)
                {
                    // finish the reduction in the last thread
                    for(int i=1, n=stackTop.fetchAndAddOrdered(0); i<n; ++i)
                        redStack[0]=redF(redStack[0], redStack[i]);
                    // kill the useless elements
                    redStack.resize(1);
                    this->completion.release(this->in.size());
                }
            }
        }
    }

    MapReduceJob(
            const std::vector<T>& in,
            const TMapF &mapF,
            const TRedF &redF,
            unsigned int threads,
            QThreadPool *owner
            )
        : BaseMapJob<T, U, TMapF, U>(in, mapF, owner), redF(redF)
    {
        // Prepare the stack where values reduced by each thread are stored;
        // keep an extra element for the (buggy) case where the thread count
        // of the thread pool has been increased after the job start and we
        // are in recursive mode.
        // In this case, we may enter %doRun %threads + 1 times, +1 being due
        // to %runIfRecursive through %result
        redStack.resize(threads + 1);
    }

    virtual const U &result()
    {
        this->waitForFinish();
        return redStack.front();
    }
};

}

/**
 * Returns the default thread pool for the current thread
 */
inline QThreadPool *defaultThreadPool()
{
    if(Impl::defaultThreadPool.hasLocalData())
    {
        if(QThreadPool *tp = *Impl::defaultThreadPool.localData())
            return tp;
    }
    return QThreadPool::globalInstance();
}

/**
 * The %Future class encapsulates a running asynchronous operation, which will
 * return a result of type %U
 *
 * Since it stores a QSharedPointer to the actual Job object, it is cheap to
 * copy (with implicit sharing semantics)
 */
template<typename U>
class Future
{
    QSharedPointer< Impl::Job<U> > fi;  ///< pointer to the running job
public:
    Future(QSharedPointer< Impl::Job<U> > fi)
        : fi(fi) {}

    /**
     * Blocks until the result is ready and returns it
     */
    const U &result()
    {
        // delegate to the Job
        return fi->result();
    }
};

template<>
class Future<void>
{
    QSharedPointer< Impl::Job<void> > fi;  ///< pointer to the running job
public:
    Future(QSharedPointer< Impl::Job<void> > fi)
        : fi(fi) {}

    /**
     * Blocks until the result is ready and returns it
     */
    void result()
    {
        fi->result();
    }
};


/**
 * Concurrently maps (via %mapF) the data of type %T stored in %in to type %U
 *
 * Normally only the template parameter %U has to be specified
 *
 * @param[in] in                Input data; if empty, the result will be an empty vector
 * @param[in] mapF              Mapping function
 * @param[in] threadPool    Thread pool to use; uses %defaultThreadPool() by default
 * @return A %Future< std::vector<U> >, that can be used to retrieve later the result.
 */
template<typename U, typename T, typename TMapF>
Future<std::vector<U> > map(const std::vector<T> in, const TMapF &mapF, QThreadPool *threadPool=MapReduce::defaultThreadPool())
{
    using namespace Impl;
    int threads=std::max(threadPool->maxThreadCount(), 1);
    QSharedPointer<MapJob<T, U, TMapF> > ret(new MapJob<T, U, TMapF>(in, mapF, threadPool));
    if(in.size())
    {
        for(int i=0; i<threads; ++i)
            threadPool->start(new BareRunnableWrapper(ret));
    }
    return Future< std::vector<U> >(ret);
}

/**
 * Concurrently calls %fn over each element of %in.
 *
 * @param[in] in            Input data
 * @param[in] fn            Mapping function
 * @param[in] threadPool    Thread pool to use; uses %defaultThreadPool() by default
 * @return A %Future<void>, that can be used to wait for the operation to finish.
 */
template<typename T, typename TFn>
Future<void> forEach(const std::vector<T> in, const TFn &fn, QThreadPool *threadPool=defaultThreadPool())
{
    using namespace Impl;
    int threads=std::max(threadPool->maxThreadCount(), 1);
    QSharedPointer<MapJob<T, void, TFn> > ret(new MapJob<T, void, TFn>(in, fn, threadPool));
    if(in.size())
    {
        for(int i=0; i<threads; ++i)
            threadPool->start(new BareRunnableWrapper(ret));
    }
    return Future <void> (ret);
}

/**
 * Concurrently maps (via %mapF) and reduces (via %redF) the data ostored in %in
 *
 * Normally only the template parameter %U has to be specified
 *
 * @param[in] in            Input data; if empty, the result will be a default-constructed %U
 * @param[in] mapF          Mapping function
 * @param[in] redF          Reduce function
 * @param[in] threadPool    Thread pool to use; uses %defaultThreadPool() by default
 * @return A %Future<U>, that can be used to retrieve later the result.
 */
template<typename U, typename T, typename TMapF, typename TRedF>
Future<U> mapReduce(const std::vector<T> in, const TMapF &mapF, const TRedF &redF, QThreadPool *threadPool=defaultThreadPool())
{
    using namespace Impl;
    int threads=std::max(threadPool->maxThreadCount(), 1);
    QSharedPointer<MapReduceJob<T, U, TMapF, TRedF> > ret(new MapReduceJob<T, U, TMapF, TRedF>(in, mapF, redF, threads, threadPool));
    if(in.size())
    {
        for(int i=0; i<threads; ++i)
            threadPool->start(new BareRunnableWrapper(ret));
    }
    return Future<U>(ret);
}

}

#endif
